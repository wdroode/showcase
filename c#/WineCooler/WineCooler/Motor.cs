﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace WineCooler
{
    class Motor
    {
        // Temperature decrease per second when motor is on
        private const double DECR_TEMP = -0.00066666666;

        // Temperature increase when motor is off
        private const double INCR_TEMP_OFF = 0.00277777777;

        // Temperature increase when door is open
        private const double INCR_TEMP_OPEN = 0.015;

        // 1 second Interval in milliseconds
        private const int INTERVAL = 1000;

        // Thermometer class reference
        public Thermometer thermometer = new Thermometer();

        // Timer class reference
        private Timer timer = new Timer();                

        // Default constructor
        public Motor()
        {

        }

        
        public void eventTimer()
        {
            timer.Elapsed += new ElapsedEventHandler(eventHandler);
            timer.Interval = INTERVAL; // 1 second by default
            timer.Enabled = true;
        }

        // Handles events when called by timer
        private void eventHandler(object source, ElapsedEventArgs e)
        {
            adjustTemperature();
            Console.WriteLine(thermometer.CURRTEMP);
        }

        private void adjustTemperature()
        {             
            // Check if motor is off and door is open    
            if (thermometer.MOTORSTATUS == false && thermometer.DOORSTATUS == true)
            {
                // Increase current temperature
                thermometer.CURRTEMP += (INCR_TEMP_OFF + INCR_TEMP_OPEN);
                isPrefTemp();
            }

            // Check if motor is on and door is open
            if (thermometer.MOTORSTATUS == true && thermometer.DOORSTATUS == true)
            {
                // Increase current temperature
                thermometer.CURRTEMP += (DECR_TEMP + INCR_TEMP_OPEN);
                isPrefTemp();
            }

            // Check if motor is on and door is closed
            if (thermometer.MOTORSTATUS == true && thermometer.DOORSTATUS == false)
            {
                // Decrease current temperature
                thermometer.CURRTEMP += DECR_TEMP;
                isPrefTemp();
            }    
            
            // Check if motor is off and door is closed
            if (thermometer.MOTORSTATUS == false && thermometer.DOORSTATUS == false)
            {
                // Increase current temperature
                thermometer.CURRTEMP += INCR_TEMP_OFF;
                isPrefTemp();
            }             
        }

        // Check temperature and turn motor on or off
        private void isPrefTemp()
        {
            if (thermometer.PREFTEMP < thermometer.CURRTEMP)
                thermometer.MOTORSTATUS = true;
            else if (thermometer.PREFTEMP >= thermometer.CURRTEMP)
                thermometer.MOTORSTATUS = false;
        }   
        
    }
}
