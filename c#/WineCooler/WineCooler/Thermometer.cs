﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WineCooler
{
    class Thermometer
    {
        // Room temperature
        private const double room_temp = 21.0;

        // Default prefered temperature
        private const double default_pref_temp = 7.2;

        // Prefered temperature set by user
        private double prefTemp = 7.2;

        // Current temperature, starts at room temperature
        private double currTemp;

        // Door: true = closed, false = opened
        private bool doorStatus = false;

        // Motor: true = on, false = off
        private bool motorStatus = true;

        // Constructor
        public Thermometer()
        {
            this.currTemp = ROOM_TEMP;
        }

        // Getters & setters
        public double PREFTEMP
        {
            get
            {
                return this.prefTemp;
            }
            set
            {
                this.prefTemp = value;
            }
        }

        public double CURRTEMP
        {
            get
            {
                return this.currTemp;
            }
            set
            {
                this.currTemp = value;
            }
        }

        public bool DOORSTATUS
        {
            get
            {
                return this.doorStatus;
            }
            set
            {
                this.doorStatus = value;
            }
        }

        public bool MOTORSTATUS
        {
            get
            {
                return this.motorStatus;
            }
            set
            {
                this.motorStatus = value;
            }
        }

        public double ROOM_TEMP
        {
            get
            {
                return room_temp;
            }
        }
            
    }
}
