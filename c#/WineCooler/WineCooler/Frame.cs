﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WineCooler
{
    public partial class Frame : Form
    {
        // Motor class refference
        Motor motor = new Motor();
        
        // Constructor
        public Frame()
        {
            InitializeComponent();
            System.Windows.Forms.Timer RefreshTimer = new System.Windows.Forms.Timer();
            RefreshTimer.Interval = 50;
            RefreshTimer.Tick += new System.EventHandler(eventHandler);
            RefreshTimer.Start();
            motor.eventTimer();
        }

        // Handles events when called by timer
        private void eventHandler(object sender, EventArgs e)
        {
            refreshLabels();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double temp = 0;
            temp = Convert.ToDouble(this.textBox1.Text);

            this.label_prefTemp.Text = temp.ToString();
            motor.thermometer.PREFTEMP = temp;

        }

        // Refresh labels on form
        private void refreshLabels()
        {            
            this.label_prefTemp.Text = String.Format("{0:0.##}", motor.thermometer.PREFTEMP.ToString());
            this.label2.Text = String.Format("{0:0.##}", motor.thermometer.CURRTEMP.ToString());

            if (motor.thermometer.MOTORSTATUS)
            {
                this.label4.Text = "ON";
            } else if (!motor.thermometer.MOTORSTATUS)
            {
                this.label4.Text = "OFF";
            }

            if (motor.thermometer.DOORSTATUS)
            {
                this.label6.Text = "OPEN";
            }
            else if (!motor.thermometer.DOORSTATUS)
            {
                this.label6.Text = "CLOSED";
            }

        }

        // Set door open or closed
        private void button2_Click(object sender, EventArgs e)
        {
            if (motor.thermometer.DOORSTATUS)
                motor.thermometer.DOORSTATUS = false;
            else if (!motor.thermometer.DOORSTATUS)
                motor.thermometer.DOORSTATUS = true;
        }
    }
}
