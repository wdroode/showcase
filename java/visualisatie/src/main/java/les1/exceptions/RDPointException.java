package les1.exceptions;

/**
 * Created by Wesley on 15-5-2016.
 */
public class RDPointException extends Exception {

    public RDPointException() {
        super();
    }

    public RDPointException(String message) {
        super(message);
    }

    public RDPointException(String message, Throwable cause) {
        super(message, cause);
    }

    public RDPointException(Throwable cause) {
        super(cause);
    }

}
