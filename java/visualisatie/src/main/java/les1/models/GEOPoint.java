package les1.models;

/**
 * Created by Wesley on 15-5-2016.
 */
public class GEOPoint {
    private boolean positive;
    private double degrees, minutes, seconds;

    public GEOPoint() {
    }

    public GEOPoint(boolean positive, double degrees, double minutes, double seconds) {
        this.positive = positive;
        this.degrees = degrees;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public boolean isPositive() {
        return positive;
    }

    public void setPositive(boolean positive) {
        this.positive = positive;
    }

    public double getDegrees() {
        return degrees;
    }

    public void setDegrees(double degrees) {
        this.degrees = degrees;
    }

    public double getMinutes() {
        return minutes;
    }

    public void setMinutes(double minutes) {
        this.minutes = minutes;
    }

    public double getSeconds() {
        return seconds;
    }

    public void setSeconds(double seconds) {
        this.seconds = seconds;
    }

    @Override
    public String toString() {
        return "GEOPoint{" +
                "\npositive: " + positive +
                "\ndegrees: " + degrees +
                "\nminutes: " + minutes +
                "\nseconds: " + seconds +
                "\n}";
    }

}
