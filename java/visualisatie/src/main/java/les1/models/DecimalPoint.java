package les1.models;

/**
 * Created by Wesley on 15-5-2016.
 */
public class DecimalPoint {

    private double decimal;

    public DecimalPoint() {
    }

    public DecimalPoint(double decimal) {
        this.decimal = decimal;
    }

    public double getDecimal() {
        return decimal;
    }

    public void setDecimal(double decimal) {
        this.decimal = decimal;
    }

    @Override
    public String toString() {
        return "DecimalPoint{" +
                "\ndecimal: " + decimal +
                "\n}";
    }

}
