package les1.models;

/**
 * Created by Wesley on 15-5-2016.
 */
public class Point<T> {

    private T x, y;

    public Point() {
    }

    public Point(T x, T y) {
        this.x = x;
        this.y = y;
    }

    public T getX() {
        return x;
    }

    public void setX(T x) {
        this.x = x;
    }

    public T getY() {
        return y;
    }

    public void setY(T y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point{" +
                "\nx: " + x.toString() +
                "\ny: " + y.toString() +
                "\n}";
    }

}

