package les1.models;

import les1.exceptions.RDPointException;

/**
 * Created by Wesley on 15-5-2016.
 */
public class RDPoint {

    private double number;

    public RDPoint(double number) throws RDPointException {

        if (number > 0 || number < 620000) {
            this.number = number;
        } else {
            throw new RDPointException("Number must be > 0 and < 620");
        }
    }

    public RDPoint() {
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "RDPoint{" +
                "\nnumber: " + number +
                "\n}";
    }

}