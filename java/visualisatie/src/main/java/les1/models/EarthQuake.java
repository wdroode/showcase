package les1.models;

import java.util.Date;

import tools.StringToDate;
import org.json.simple.JSONObject;

/**
 * Created by Wesley on 12-5-2016.
 */
public class EarthQuake {

    private Date timestamp;
    private float latitude;
    private float longtitude;
    private float depth;
    private float size;
    private float quality;
    private String humanReadableLocation;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(float longtitude) {
        this.longtitude = longtitude;
    }

    public float getDepth() {
        return depth;
    }

    public void setDepth(float depth) {
        this.depth = depth;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public float getQuality() {
        return quality;
    }

    public void setQuality(float quality) {
        this.quality = quality;
    }

    public String getHumanReadableLocation() {
        return humanReadableLocation;
    }

    public void setHumanReadableLocation(String humanReadableLocation) {
        this.humanReadableLocation = humanReadableLocation;
    }

    public int getRichterIndication() {
        int indication = 200;

        if (this.size >= 0.3) {
            indication = 0;
        }

        return indication;
    }

    public void JsonToClass(JSONObject object) {
        setTimestamp(StringToDate.convert((String) object.get("timestamp")));
        setLatitude(((Number) object.get("latitude")).floatValue());
        setLongtitude(((Number) object.get("longitude")).floatValue());
        setDepth(((Number) object.get("depth")).floatValue());
        setSize(((Number) object.get("size")).floatValue());
        setQuality(((Number) object.get("quality")).floatValue());
        setHumanReadableLocation(((String) object.get("humanReadableLocation")));
    }

}
