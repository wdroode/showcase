package les1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import processing.core.PApplet;
import processing.core.PImage;

import les1.models.EarthQuake;
import tools.SimpleJsonParser;


public class Main extends PApplet {

    private String backgroundImagePath = "resc/ijsland-kaart.png";
    private String jsonFilePath = "resc/ijsland-metingen.json";
    private String jsonArray = "results";
    private String title = "Aardbevingen in Ijsland";

    private PImage backgroundImage;

    // Map dimensions
    private float yMax = 67.3f;
    private float yMin = 63.0f;
    private float xMin = -25.7f;
    private float xMax = -12.0f;

    private static List<EarthQuake> earthQuakes = new ArrayList<EarthQuake>();

    public void settings() {
        size(1000, 812); // size of background image
    }

    public void setup() {
        this.backgroundImage = loadImage(this.backgroundImagePath);
    }

    public void draw() {
        surface.setTitle(this.title);
        background(255, 255, 255);
        image(backgroundImage, 0, 0);
        drawEarthquakes();
    }

    public static void main(String[] args) {
        PApplet.main(new String[]{Main.class.getName()});

        new Main().loadEarthQuakes();

        Collections.sort(earthQuakes, COMPARATOR); // Sort earthquakes in size
    }

    private void loadEarthQuakes() {

        try {
            SimpleJsonParser parser = new SimpleJsonParser(this.jsonFilePath);

            parser.setJsonArray(this.jsonArray);

            for (Object object : parser.getJsonArray()) {
                JSONObject jsonObject = (JSONObject) object;
                EarthQuake earthQuake = new EarthQuake();
                earthQuake.JsonToClass(jsonObject);

                earthQuakes.add(earthQuake);
            }

        } catch (IOException exception) {
            exception.printStackTrace();
        } catch (ParseException exception) {
            exception.printStackTrace();
        }
    }

    private static Comparator<EarthQuake> COMPARATOR = new Comparator<EarthQuake>() {
        public int compare(EarthQuake o1, EarthQuake o2) {
            int i = 0;
            float size1 = o1.getSize();
            float size2 = o2.getSize();

            if (size1 > size2) i = -1;
            if (size1 < size2) i = 1;

            return i;
        }
    };

    private void drawEarthquakes() {
        for (EarthQuake earthQuake : earthQuakes) {
            fill(255, 255 - (earthQuake.getDepth() * 20), 0);

            ellipse(
                    map(earthQuake.getLongtitude(), xMin, xMax, 0, backgroundImage.width),
                    map(earthQuake.getLatitude(), yMax, yMin, 0, backgroundImage.height),
                    earthQuake.getSize() * 25,
                    earthQuake.getSize() * 25
            );

        }
    }

}

