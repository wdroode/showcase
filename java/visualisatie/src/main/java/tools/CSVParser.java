package tools;

import les2.Models.CartesianCoordinate;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wesley on 30-5-2016.
 */
public class CSVParser {
    private int lineCount = 1;
    private String separator = ",";

    /**
     * Does not work for this assignment because the file is too big, See function 'parseDirectlyToObject' below
     */
    public List<String[]> parse(String filePath) {
        List<String[]> result = new ArrayList<>();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
            String line;

            while ((line = bufferedReader.readLine()) != null && this.lineCount < 1000) {

                if (this.lineCount > 0) {
                    result.add(line.split(this.separator));
                }

                this.lineCount++;
            }

            System.out.println("Parsing complete: " + this.lineCount + " lines of data read.");
            bufferedReader.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return result;
    }

    public CSVParser skipFirstLine() {
        this.lineCount = 0;

        return this;
    }

    public CSVParser separator(String separator) {
        this.separator = separator;

        return this;
    }

    /**
     * Parsing directly to CartesianCoordinate object
     */
    public List<CartesianCoordinate> parseDirectlyToObject(String filePath) {
        List<CartesianCoordinate> results = new ArrayList<>();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
            String line;
            int count = 0;

            while ((line = bufferedReader.readLine()) != null && this.lineCount < 1000) {

                if (count > 0) {
                    results.add(new CartesianCoordinate(
                            Float.parseFloat(line.split(this.separator)[0]),
                            Float.parseFloat(line.split(this.separator)[1]),
                            Float.parseFloat(line.split(this.separator)[2])
                    ));
                }

                count++;
            }

            System.out.println("Parsing complete: " + count + " lines of data read.");
            bufferedReader.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return results;
    }

}
