package tools;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 * Created by Wesley on 12-5-2016.
 */
public class SimpleJsonParser extends JSONParser {

    private JSONObject jsonObject;
    private JSONArray jsonArray;

    public SimpleJsonParser(String json) throws IOException, ParseException {
        this.jsonObject = (JSONObject)parse(new FileReader(json));
    }

    public void setJsonArray(String key) {
        this.jsonArray = (JSONArray)this.jsonObject.get(key);
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }
}
