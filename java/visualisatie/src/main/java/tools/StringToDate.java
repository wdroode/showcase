package tools;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Wesley on 15-5-2016.
 */
public class StringToDate {

    public static Date convert(String rawDate) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;

        try {
            date = dateFormat.parse(getDateFromString(rawDate));
        } catch(ParseException exception) {
            exception.printStackTrace();
        }

        return date;
    }

    /**
     * Converts date from json to readable Java format
     * @param jsonDate
     * @return Java formattable date as string
     */
    public static String getDateFromString(String jsonDate) {
        String date = jsonDate.substring(0, 10);
        String time = jsonDate.substring(11, 19);

        return date + " " + time;
    }

}
