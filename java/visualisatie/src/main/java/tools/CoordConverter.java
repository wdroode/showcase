package tools;

import les1.exceptions.RDPointException;

import les1.models.DecimalPoint;
import les1.models.GEOPoint;
import les1.models.Point;
import les1.models.RDPoint;
import les2.Models.Axis;
import les2.Models.CartesianCoordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wesley on 15-5-2016.
 */
public class CoordConverter {
    private static RDPoint rdPointX, rdPointY;

    public static Point<DecimalPoint> RdPointToDecimalPoint(Point<RDPoint> coordinates) {
        return GEOPointToDecimalPoint(RdToGeographic(coordinates));
    }

    public static Point<RDPoint> DecimalPointToRdPoint(Point<DecimalPoint> coordinates) {
        return GeographicToRd(decimalPointToGEOPoint(coordinates));
    }

    public static Point<DecimalPoint> GEOPointToDecimalPoint(Point<GEOPoint> coordinates) {
        return new Point<DecimalPoint>(degreesMinutesSecondsToDegrees(coordinates.getX()), degreesMinutesSecondsToDegrees(coordinates.getY()));
    }

    public static Point<GEOPoint> decimalPointToGEOPoint(Point<DecimalPoint> coordinates) {
        return new Point<GEOPoint>(degreesToDegreesMinutesSeconds(coordinates.getX()), degreesToDegreesMinutesSeconds(coordinates.getY()));
    }

    public static Point<RDPoint> GeographicToRd(Point<GEOPoint> coordinates) {

        // The city “Amsterfoort” is used as reference “Rd” coordinate.
        int referenceRdX = 155000;
        int referenceRdY = 463000;

        // The city “Amsterfoort” is used as reference “WGS84” coordinate.
        double referenceWgs84X = 52.15517;
        double referenceWgs84Y = 5.387206;

        double[][] Rpq = new double[4][5];

        Rpq[0][1] = 190094.945;
        Rpq[1][1] = -11832.228;
        Rpq[2][1] = -114.221;
        Rpq[0][3] = -32.391;
        Rpq[1][0] = -0.705;
        Rpq[3][1] = -2.340;
        Rpq[0][2] = -0.008;
        Rpq[1][3] = -0.608;
        Rpq[2][3] = 0.148;

        double[][] Spq = new double[4][5];
        Spq[0][1] = 0.433;
        Spq[0][2] = 3638.893;
        Spq[0][4] = 0.092;
        Spq[1][0] = 309056.544;
        Spq[2][0] = 73.077;
        Spq[1][2] = -157.984;
        Spq[3][0] = 59.788;
        Spq[2][2] = -6.439;
        Spq[1][1] = -0.032;
        Spq[1][4] = -0.054;

        double d_lattitude = (0.36 * (degreesMinutesSecondsToDegrees(coordinates.getX()).getDecimal() - referenceWgs84X));
        double d_longitude = (0.36 * (degreesMinutesSecondsToDegrees(coordinates.getY()).getDecimal() - referenceWgs84Y));

        double calc_latt = 0;
        double calc_long = 0;

        for (int p = 0; p < 4; p++) {
            for (int q = 0; q < 5; q++) {
                calc_latt += Rpq[p][q] * Math.pow(d_lattitude, p) * Math.pow(d_longitude, q);
                calc_long += Spq[p][q] * Math.pow(d_lattitude, p) * Math.pow(d_longitude, q);
            }
        }

        double rd_x_coordinate = (referenceRdX + calc_latt);
        double rd_y_coordinate = (referenceRdY + calc_long);

        try {
            rdPointX = new RDPoint(rd_x_coordinate);
            rdPointY = new RDPoint(rd_y_coordinate);
        } catch(RDPointException exception) {
            exception.printStackTrace();
        }

        return new Point(rdPointX, rdPointY);

    }

    public static Point<GEOPoint> RdToGeographic(Point<RDPoint> coordinates) {
        // The city "Amsterfoort" is used as reference "Rd" coordinate.
        int referenceRdX = 155000;
        int referenceRdY = 463000;

        double dX = (double) (coordinates.getX().getNumber() - referenceRdX) * Math.pow(10, -5);
        double dY = (double) (coordinates.getY().getNumber() - referenceRdY) * Math.pow(10, -5);

        double sumN =
                (3235.65389 * dY) +
                        (-32.58297 * Math.pow(dX, 2)) +
                        (-0.2475 * Math.pow(dY, 2)) +
                        (-0.84978 * Math.pow(dX, 2) * dY) +
                        (-0.0655 * Math.pow(dY, 3)) +
                        (-0.01709 * Math.pow(dX, 2) * Math.pow(dY, 2)) +
                        (-0.00738 * dX) +
                        (0.0053 * Math.pow(dX, 4)) +
                        (-0.00039 * Math.pow(dX, 2) * Math.pow(dY, 3)) +
                        (0.00033 * Math.pow(dX, 4) * dY) +
                        (-0.00012 * dX * dY);
        double sumE =
                (5260.52916 * dX) +
                        (105.94684 * dX * dY) +
                        (2.45656 * dX * Math.pow(dY, 2)) +
                        (-0.81885 * Math.pow(dX, 3)) +
                        (0.05594 * dX * Math.pow(dY, 3)) +
                        (-0.05607 * Math.pow(dX, 3) * dY) +
                        (0.01199 * dY) +
                        (-0.00256 * Math.pow(dX, 3) * Math.pow(dY, 2)) +
                        (0.00128 * dX * Math.pow(dY, 4)) +
                        (0.00022 * Math.pow(dY, 2)) +
                        (-0.00022 * Math.pow(dX, 2)) +
                        (0.00026 * Math.pow(dX, 5));


        // The city "Amsterfoort" is used as reference "WGS84" coordinate.
        double referenceWgs84X = 52.15517;
        double referenceWgs84Y = 5.387206;

        DecimalPoint latitude = new DecimalPoint(referenceWgs84X + (sumN / 3600));
        DecimalPoint longitude = new DecimalPoint(referenceWgs84Y + (sumE / 3600));

        return new Point<GEOPoint>(degreesToDegreesMinutesSeconds(latitude),
                degreesToDegreesMinutesSeconds(longitude));
    }

    private static DecimalPoint degreesMinutesSecondsToDegrees(GEOPoint coordinate) {
        return new DecimalPoint(coordinate.getDegrees() + (coordinate.getMinutes() / 60) + (coordinate.getSeconds() / 3600));
    }

    private static GEOPoint degreesToDegreesMinutesSeconds(DecimalPoint coordinate) {
        double degreesCoordinate = coordinate.getDecimal() - (coordinate.getDecimal() % 1);
        double decimalCoordinate = coordinate.getDecimal() - degreesCoordinate;

        double minutesCoordinate = (decimalCoordinate * 60) - ((decimalCoordinate * 60) % 1);
        double secondsCoordinate = ((decimalCoordinate * 60) % 1) * 60;

        GEOPoint dms = new GEOPoint();
        dms.setDegrees(degreesCoordinate);
        dms.setMinutes(minutesCoordinate);
        dms.setSeconds(secondsCoordinate);

        if (degreesCoordinate < 0)
            dms.setPositive(false);
        else
            dms.setPositive(true);

        return dms;

    }

    /**
     * Filters needed coordinates to create dimensions
     */
    public List<CartesianCoordinate> filterCoordinates(List<CartesianCoordinate> coordinates, int size, CartesianCoordinate center) {
        List<CartesianCoordinate> result = new ArrayList<>();

        for (CartesianCoordinate coordinate : coordinates) {
            float x = Math.abs(center.getX() - coordinate.getX());
            float y = Math.abs(center.getY() - coordinate.getY());

            if (x < size && y < size) {
                result.add(coordinate);
            }
        }

        return result;
    }

    /**
     * Set min max Axis
     */
    public Axis setMinMax(List<CartesianCoordinate> coordinates) {
        Axis axis = new Axis(
                coordinates.stream()
                        .min((o1, o2) -> Float.compare(o1.getX(), o2.getX()))
                        .get()
                        .getX(),

                coordinates.stream()
                        .min((o1, o2) -> Float.compare(o1.getY(), o2.getY()))
                        .get()
                        .getY(),

                coordinates.stream()
                        .min((o1, o2) -> Float.compare(o1.getZ(), o2.getZ()))
                        .get()
                        .getZ(),

                coordinates.stream()
                        .max((o1, o2) -> Float.compare(o1.getX(), o2.getX()))
                        .get()
                        .getX(),

                coordinates.stream()
                        .max((o1, o2) -> Float.compare(o1.getY(), o2.getY()))
                        .get()
                        .getY(),

                coordinates.stream()
                        .max((o1, o2) -> Float.compare(o1.getZ(), o2.getZ()))
                        .get()
                        .getZ()
        );

        return axis;
    }
}
