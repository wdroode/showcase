package les2;

import les2.Models.Axis;
import les2.Models.CartesianCoordinate;
import tools.CSVParser;

import processing.core.PApplet;
import processing.core.PVector;
import processing.event.MouseEvent;
import tools.CoordConverter;

import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Wesley on 30-5-2016.
 */
public class Main extends PApplet {
    private final String TITLE = "Rotterdam FLood Simulation";
    private final String FILE_PATH = "F:\\Code\\resc\\rotterdamopendata_hoogtebestandtotaal_oost.csv"; // http://med.hro.nl/grobj/Hoogtedata/
    private final CartesianCoordinate ZADKINE_STATUE_COORDINATES = new CartesianCoordinate(92796f, 436960f, 0);

    private List<CartesianCoordinate> coordinates;
    private List<CartesianCoordinate> smallCoordinates; // 500x500
    private List<CartesianCoordinate> bigCoordinates;   // 1000x1000
    private List<PVector> drawingPoints;

    private Axis axis = new Axis();
    private float scale = 1f;
    private float waterHeight = 0f;
    private float floodSpeed = 0.5f;
    private float timer = 1f;
    private int size = 500;

    private PVector angle = new PVector(1.2f, 0f, 5.5f);

    private boolean pause = true;
    private boolean doInit = false;

    private CoordConverter converter = new CoordConverter();

    public static void main(String[] args) {
        PApplet.main(new String[]{Main.class.getName()});
    }

    public void settings() {
        size(1000, 1000, P3D); // added P3D for 3d rendering
    }

    public void setup() {
        surface.setTitle(this.TITLE);
        background(255, 0, 0);

        this.loadParsedCoordinates();

        this.smallCoordinates = this.converter.filterCoordinates(this.coordinates, 500, this.ZADKINE_STATUE_COORDINATES);
        this.bigCoordinates = this.converter.filterCoordinates(this.coordinates, 1000, this.ZADKINE_STATUE_COORDINATES);
        this.coordinates = this.smallCoordinates; // set 500x500 as default by on startup

        this.axis = converter.setMinMax(this.coordinates);
        this.waterHeight = axis.getMinZ();
        this.drawingPoints = this.setDrawingPoints();
    }

    public void draw() {
        this.isInit();

        clear();
        lights(); // adds lighting for more depth

        this.drawHelp();

        translate(width / 2, height / 2);

        rotateX(this.angle.x);
        rotateZ(this.angle.z);

        pushMatrix();

        this.drawMap();
        this.drawFlood();

        popMatrix();
    }


    private void loadParsedCoordinates() {
        CSVParser csvParser = new CSVParser();
        this.coordinates = csvParser.skipFirstLine().parseDirectlyToObject(this.FILE_PATH);
    }

    private List<PVector> setDrawingPoints() {
        List<PVector> drawingPoints = new ArrayList<>();

        for (CartesianCoordinate coordinate : this.coordinates) {
            drawingPoints.add(
                    new PVector(
                            map(coordinate.getX(), this.axis.getMinX(), this.axis.getMaxX(), 0, (1000)),
                            map(coordinate.getY(), this.axis.getMinY(), this.axis.getMaxY(), 0, (1000)),
                            coordinate.getZ()
                    )
            );
        }

        return drawingPoints;
    }

    /**
     * Methods to draw elements to screen
     */
    private void drawMap() {
        noStroke();
        for (PVector drawingPoint : this.drawingPoints) {
            fill(this.createRGB(drawingPoint.z));
            pushMatrix();
            translate((drawingPoint.x - 500f) * scale, (1000f - drawingPoint.y - 500f) * scale, 0);
            box(2 * scale, 2 * scale, ((drawingPoint.z + 10) * 2 * scale));
            popMatrix();
        }
    }

    // Increase color for more depth
    private int createRGB(float increase) {
        float incBy = increase * 20;
        return color(30 + incBy, 30 + incBy, 37 + incBy);
    }

    private void drawFlood() {
        if (!this.pause && this.waterHeight <= this.axis.getMaxZ()) {
            this.waterHeight += this.floodSpeed;
            this.timer += this.floodSpeed * 2f;
        }

        fill(0, 0, 255, 95);
        pushMatrix();
        translate(0, 0, 0);
        box((this.size * 2) * scale, (this.size * 2) * scale, (this.waterHeight + 10) * 2 * scale);
        popMatrix();
    }

    public void drawHelp() {
        textSize(15);
        fill(176, 196, 222);
        text("Press ENTER to start/pause", 20, 30);
        text("Press BACKSPACE to resetFlood", 20, 60);
        text("Press UP/DOWN to adjust the speed", 20, 90);
        text("Press P to take a screenshot", 20, 120);
        text("Press S to change size, " + "Current size: " + this.size + "x" + this.size + " m2", 20, 150);
        text("Current water height: " + String.format("%.1f", this.waterHeight) + "m, " +
                "Speed: " + String.format("%.1f", this.floodSpeed) + ", Hours passed: " + String.format("%.1f", this.timer), 20, 180);

    }

    /**
     * Methods to switch size and reinitialize map
     */
    private void switchSize() {
        if (this.size == 500) {
            this.size = 1000;
            this.coordinates = this.bigCoordinates;

        } else {
            this.size = 500;
            this.coordinates = this.smallCoordinates;
        }

        this.doInit = true;
    }


    // Check if reinitialization is needed
    private void isInit() {
        if (this.doInit) {
            this.init();
            this.doInit = false;
        }
    }

    private void init() {
        this.converter.setMinMax(this.coordinates);
        this.waterHeight = this.axis.getMinZ();
        this.drawingPoints = this.setDrawingPoints();
    }

    /**
     * Button assignments & mouse actions
     */
    private void resetFlood() {
        this.waterHeight = this.axis.getMinZ();
        this.timer = 0f;
    }

    private void pauseFlood() {
        if (!this.pause)
            this.pause = true;
        else
            this.pause = false;
    }

    private void prtScn() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        String date = dateFormat.format(currentDate);
        saveFrame("###-screen-shot-" + date + ".png");
        System.out.println("Saved:" + "screen-shot-" + date + ".png");
    }

    private void increaseFloodSpeed() {
        if (this.floodSpeed <= 2f) {
            this.floodSpeed += 0.1f;
        }
    }

    private void decreaseFloodSpeed() {
        if (this.floodSpeed >= 0.2f) {
            this.floodSpeed -= 0.1f;
        }
    }

    @Override
    public void mouseDragged() {
        if (mouseButton == RIGHT) {
            this.angle.x += (radians(-(mouseY - pmouseY)) / 2);
            this.angle.z += (radians(-(mouseX - pmouseX)) / 2);
        }
    }

    @Override
    public void mouseWheel(MouseEvent mouseEvent) {
        float count = mouseEvent.getCount();

        this.scale -= (count / 5.0);

        if (this.scale < 1)
            this.scale = 1;
        if (this.scale > 3)
            this.scale = 3;
    }

    @Override
    public void keyPressed() {
        switch (keyCode) {
            case KeyEvent.VK_BACK_SPACE:
                this.resetFlood();
                break;
            case KeyEvent.VK_ENTER:
                this.pauseFlood();
                break;
            case KeyEvent.VK_P:
                this.prtScn();
                break;
            case KeyEvent.VK_UP:
                this.increaseFloodSpeed();
                break;
            case KeyEvent.VK_DOWN:
                this.decreaseFloodSpeed();
                break;
            case KeyEvent.VK_S:
                this.switchSize();
                break;
        }
    }

}
