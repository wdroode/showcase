package les2.Models;

/**
 * Created by Wesley on 30-5-2016.
 */
public class CartesianCoordinate implements ParseAbleInterface {
    private float x, y, z;

    public CartesianCoordinate(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public CartesianCoordinate constructFromStrings(String values[]) {
        this.x = Float.parseFloat(values[0]);
        this.y = Float.parseFloat(values[1]);
        this.z = Float.parseFloat(values[2]);

        return this;
    }

    @Override
    public String toString() {
        return "CartesianCoordinate {" +
                "\nX: " + x +
                "\nY: " + y +
                "\nZ: " + z +
                "\n}\n";
    }
}
