package les2.Models;

/**
 * Created by Wesley on 31-5-2016.
 */
public interface ParseAbleInterface {
    ParseAbleInterface constructFromStrings(String values[]);
}
