# README #



### Huiswerkopdrachten ###

* C#
* Java
* PHP

### C# ###

Wine cooler was een opdracht voor het keuzevak dotnet en moet de werking van een wijnkoeler in code voorstellen.

### Java ###

Deze opdrachten in processing bestaan uit les1, waarbij de coordinaten en de sterkte van aardbevingen op een kaart van Ijsland worden geplot. En les2 waarbij een simulatie van een overstroming in Rotterdam wordt gevisualiseerd.  

### PHP ###

Portfolio is een privé project als zelfstudie voor PHP en het framework Laravel. Project56 was een schoolproject in opdracht van CityGis waarbij we een externe datastream moesten opvangen om vervolgens naar een database te schrijven. Het doel van dit project was het vervolgens verwerken en tonen van deze data in een webapplicatie. Omdat CityGis geen datastream heeft aangeleverd is het uiteindelijk een fancy CSV parser geworden. 