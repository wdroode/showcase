<?php

namespace App\Http\Controllers\Site;

use App\Wderoode\BaseController;

class BlogController extends BaseController
{

    public function getBlog()
    {
        return view('pages.blog.blog_page', [
            'title' => 'Blog'
        ]);
    }
}