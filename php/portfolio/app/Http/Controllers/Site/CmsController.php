<?php

namespace App\Http\Controllers\Site;

use App\Wderoode\BaseController;

class CmsController extends BaseController
{

    public function getGoalsCms()
    {
        return view('cms.goals.goals_cms', [
            'title' => 'Leerdoelen Contentmanager'
        ]);
    }
}