<?php

namespace App\Http\Controllers\Site;

use App\Wderoode\BaseController;

class HomeController extends BaseController
{

    public function getHome()
    {
        return view('pages.home.home_page', [
            'title' => 'Wesley de Roode',
            'subtitle' => 'Student Informatica aan de Hogeschool Rotterdam.'
        ]);
    }
}