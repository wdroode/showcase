<?php

namespace App\Http\Controllers\Site;

use App\Wderoode\BaseController;

use App\Wderoode\Repositories\GoalRepository;
use App\Wderoode\Repositories\LabelRepository;

class GoalController extends BaseController
{

    public function getGoals()
    {
        $goals = GoalRepository::getGoalsWithRelations();

        $labels = LabelRepository::getAllLabels();

        return view('pages.goals.goals_page', [
            'title' => 'Mijn leerdoelen',
            'subtitle' => 'Hieronder vind je een tijdlijn van mijn leerdoelen en de daarbij behaalde resultaten. Gebruik de
                        filter om te sorteren op basis van categorieën.',
            'goals' => $goals,
            'labels' => $labels,
        ]);
    }

}