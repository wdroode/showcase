<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

use App\Wderoode\BaseController;

class ContactController extends BaseController
{

    public function getContact()
    {
        return view('pages.contact.contact_page', [
            'title' => 'Neem contact op!',
            'subtitle' => 'Heb je vragen? Neem gerust contact op via het formulier hieronder! Je vraag zal zo spoedig
                        mogelijk worden beantwoord.'
        ]);
    }

    public function postContact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'min:2'],
            'email' => ['required', 'email'],
            'message' => ['required'],
        ]);

        if ($validator->fails()) {
            return redirect()->route('get.contact', ['#contact-form'])->withErrors($validator)
                ->withInput($request->all())
                ->with([
                    'status' => 'Je bericht is niet verzonden, controleer je invoer.',
                    'status_type' => 'danger'
                ]);
        }

        Mail::send('emails.contact', ['request' => $request], function ($message) {
            $message->to('info@wderoode.nl');
            $message->subject('Contactformulier Wderoode.nl');
        });

        return redirect()->route('get.contact', ['#contact-form'])
            ->with([
                'status' => 'Je bericht is verzonden!',
                'status_type' => 'success'
            ]);

    }
}