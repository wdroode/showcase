<?php

namespace App\Http\Controllers\Site;

use App\Wderoode\BaseController;

class CvController extends BaseController
{

    public function getCv()
    {
        return view('pages.cv.cv_page', [
            'title' => 'Curriculum Vitae'
        ]);
    }
}