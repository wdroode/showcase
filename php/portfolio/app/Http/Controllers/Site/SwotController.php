<?php

namespace App\Http\Controllers\Site;

use App\Wderoode\BaseController;

class SwotController extends BaseController
{

    public function getSwot()
    {
        return view('pages.swot.swot_page', [
            'title' => 'Mijn SWOT-analyse',
            'subtitle' => 'Hieronder vind je mijn goede en sterke kanten in de vorm van SWOT-analyse\'s.'
        ]);
    }
}