<?php

namespace App\Http\Controllers\Site;

use App\Wderoode\BaseController;
use App\Wderoode\Repositories\CategoryRepository;
use App\Wderoode\Repositories\ProjectRepository;

class PortfolioController extends BaseController
{

    public function getPortfolio()
    {
        $categories = CategoryRepository::getAllCategories();

        $projects = ProjectRepository::getProjectsWithRelations();

        return view('pages.portfolio.portfolio_page', [
            'title' => 'Mijn portfolio',
            'subtitle' => 'Hieronder vind je al mijn projecten en opdrachten die ik in de afgelopen leerjaren heb gemaakt.',
            'categories' => $categories,
            'projects' => $projects,
        ]);
    }

}