<?php

namespace App\Http\Controllers\Auth;

use App\Wderoode\BaseController;

class AccountController extends BaseController
{

    public function getAccount() {
        return view('auth.account.account_page', [
            'title' => 'Mijn account',
            'description' => 'Verander hier je account-details'
        ]);
    }

}
