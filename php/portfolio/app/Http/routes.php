<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', ['as' => 'get.home', 'uses' => 'Site\HomeController@getHome']);

    Route::group(['prefix' => 'auth'], function () {
        Route::get('/login', ['as' => 'get.auth.login', 'uses' => 'Auth\AuthController@getLogin']);
        Route::post('/login', ['as' => 'post.auth.login', 'uses' => 'Auth\AuthController@postLogin']);
        Route::get('/logout', ['as' => 'get.auth.logout', 'uses' => 'Auth\AuthController@getLogout']);
    });

    Route::group(['prefix' => 'portfolio'], function () {
        Route::any('/', ['as' => 'get.portfolio', 'uses' => 'Site\PortfolioController@getPortfolio']);

    });

    Route::group(['prefix' => 'blog'], function () {
        Route::any('/', ['as' => 'get.blog', 'uses' => 'Site\BlogController@getBlog']);

    });

    Route::group(['prefix' => 'cv'], function () {
        Route::any('/', ['as' => 'get.cv', 'uses' => 'Site\CvController@getCv']);

    });

    Route::group(['middleware' => ['auth']], function () {
        Route::group(['prefix' => 'goals'], function () {
            Route::any('/', ['as' => 'get.goals', 'uses' => 'Site\GoalController@getGoals']);

        });

        Route::group(['prefix' => 'swot'], function () {
            Route::any('/', ['as' => 'get.swot', 'uses' => 'Site\SwotController@getSwot']);

        });

        Route::group(['prefix' => 'account'], function () {
            Route::any('/', ['as' => 'get.account', 'uses' => 'Auth\AccountController@getAccount']);

        });

        Route::group(['middleware' => ['admin']], function() {
            Route::group(['prefix' => 'cms'], function () {
                Route::any('/goals', ['as' => 'get.cms', 'uses' => 'Site\CmsController@getGoalsCms']);

            });

        });

    });

    Route::group(['prefix' => 'contact'], function () {
        Route::any('/', ['as' => 'get.contact', 'uses' => 'Site\ContactController@getContact']);
        Route::post('/', ['as' => 'post.contact', 'uses' => 'Site\ContactController@postContact']);
    });

});