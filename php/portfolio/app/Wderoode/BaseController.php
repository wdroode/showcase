<?php

namespace App\Wderoode;

use App\Wderoode\Repositories\ProjectRepository;
use App\Wderoode\Repositories\UserRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

class BaseController extends Controller
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $user = null;

    private $currentDate = null;

    private $social = null;

    private $latestProjects = null;

    public function __construct()
    {

        $this->loadUser();
        view()->share('user', $this->getUser());

        $this->loadCurrentDate();
        view()->share('currentDate', $this->getCurrentDate());

        $this->loadSocialMedia();
        View()->share('social', $this->social);

        $this->loadLatestProjects();
        View()->share('latestProjects', $this->latestProjects);

    }

    private function loadUser()
    {
        if (Auth::check()) {
            $this->user = UserRepository::getUserWithRole(Auth::User()->id);
        }
    }

    protected function getUser()
    {
        return $this->user;
    }

    private function loadCurrentDate()
    {
        $this->currentDate = Carbon::now();
    }

    private function loadSocialMedia()
    {
        $this->social = array(
            'google'    =>'https://plus.google.com/111840851061572515245/posts',
            'facebook'  =>'https://www.facebook.com/profile.php?id=100002421105240',
            'github'    => 'https://github.com/wesrd',
            'linkedin'  => 'https://nl.linkedin.com/pub/wesley-de-roode/a3/4b3/814',
            'pinterest' => 'https://nl.pinterest.com/wesleyderoode/'
        );
    }

    private function loadLatestProjects()
    {
        $this->latestProjects = ProjectRepository::getLatestProjects();
    }

    protected function getCurrentDate()
    {
        return $this->currentDate;
    }

}
