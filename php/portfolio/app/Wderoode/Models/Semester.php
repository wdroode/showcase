<?php

namespace App\Wderoode\Models;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'semesters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_year_id',
    ];

    /**
     * The attributes which represent a date.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date',
    ];

    public function schoolYear()
    {
        return $this->belongsTo(SchoolYear::class);
    }

}