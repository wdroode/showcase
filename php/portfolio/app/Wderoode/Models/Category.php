<?php

namespace App\Wderoode\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    /*
    * Relationships
    */
    public function project()
    {
        return $this->hasMany(Project::class, 'category_id');
    }
}
