<?php

namespace App\Wderoode\Models;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'goals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'label_id',
    ];

    /**
     * The attributes which represent a date.
     *
     * @var array
     */
    protected $dates = [
        'deadline',
        'created_at',
        'updated_at',
    ];

    /*
     * Relationships
     */
    public function reflection()
    {
        return $this->hasOne(Reflection::class, 'goal_id');
    }

    public function label()
    {
        return $this->belongsTo(Label::class);
    }

}