<?php

namespace App\Wderoode\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'school_years';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes which represent a date.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date',
    ];

    public function semester()
    {
        return $this->hasMany(Semester::class);
    }

}