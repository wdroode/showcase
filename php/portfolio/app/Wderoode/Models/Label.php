<?php

namespace App\Wderoode\Models;

use Illuminate\Database\Eloquent\Model;

class Label extends Model
{

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'labels';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'code',
    ];

    /*
    * Relationships
    */
    public function goal()
    {
        return $this->hasMany(Goal::class, 'label_id');
    }

}
