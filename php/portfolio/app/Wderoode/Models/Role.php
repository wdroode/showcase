<?php

namespace App\Wderoode\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /*
    * Relationships
    */
    public function user()
    {
        return $this->belongsToMany(User::class, 'user_roles');
    }

}