<?php

namespace App\Wderoode\Models;

use Illuminate\Database\Eloquent\Model;

class Reflection extends Model
{

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reflections';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text',
        'goal_id',
    ];

   /*
   * Relationships
   */
    public function goal()
    {
        return $this->belongsTo(Goal::class, 'goals');
    }

}