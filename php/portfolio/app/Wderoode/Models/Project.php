<?php

namespace App\Wderoode\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'text,',
        'client',
        'img_url',
        'thumb_url',
        'github',
        'category_id',
    ];

    /**
     * The attributes which represent a date.
     *
     * @var array
     */
    protected $dates = [
        'delivery_date',
        'created_at',
        'updated_at',
    ];

    /*
     * Relationships
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }


}
