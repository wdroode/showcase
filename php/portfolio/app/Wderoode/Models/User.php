<?php

namespace App\Wderoode\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes which represent a date.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'logged_in_at'
    ];

    /*
     * Helpers
     */
    public function updateLastLogin()
    {
        // update the last time the user is logged in
        $this->logged_in_at = Carbon::now();
        return $this->save();
    }

    /*
     * Relationships
     */
    public function role()
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

}