<?php

namespace App\Wderoode\Repositories;

use App\Wderoode\Models\User;

class UserRepository
{

    public static function getUserWithRole($id)
    {
        return User::with('role')->find($id);
    }

}