<?php

namespace App\Wderoode\Repositories;

use App\Wderoode\Models\Label;

use DB;

class LabelRepository
{

    public static function getAllLabels()
    {
        return Label::all();
    }

}