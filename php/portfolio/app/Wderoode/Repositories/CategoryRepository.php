<?php

namespace App\Wderoode\Repositories;

use App\Wderoode\Models\Category;

use DB;

class CategoryRepository
{

    public static function getAllCategories()
    {
        return Category::all();
    }

}