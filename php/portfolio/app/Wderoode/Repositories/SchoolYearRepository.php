<?php

namespace App\Wderoode\Repositories;

use App\Wderoode\Models\SchoolYear;

class SchoolYearRepository
{
    
    public static function getGoalOverview()
    {
        return SchoolYear::with('semester')
            ->with('semester.goal')
            ->has('semester.goal')
            ->get();
    }

}