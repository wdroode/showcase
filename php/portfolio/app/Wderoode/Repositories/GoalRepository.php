<?php

namespace App\Wderoode\Repositories;

use App\Wderoode\Models\Goal;

use DB;

class GoalRepository
{

    public static function getAllGoals()
    {
        return Goal::all();
    }

    public static function getGoalsWithRelations()
    {
        return Goal::query()
            ->orderBy('deadline', 'DESC')
            ->with('label')
            ->with('reflection')
            ->get();
    }

}