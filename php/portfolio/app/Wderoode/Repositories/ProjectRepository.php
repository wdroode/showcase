<?php

namespace App\Wderoode\Repositories;

use App\Wderoode\Models\Project;

use DB;

class ProjectRepository
{

    public static function getAllGoals()
    {
        return Project::all();
    }

    public static function getProjectsWithRelations()
    {
        return Project::query()
            ->orderBy('delivery_date', 'DESC')
            ->with('category')
            ->get();
    }

    public static function getLatestProjects()
    {
        return Project::query()
            ->orderBy('id', 'DESC')
            ->take(4)
            ->with('category')
            ->get();
    }

}