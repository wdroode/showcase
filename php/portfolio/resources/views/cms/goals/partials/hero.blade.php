<section id="hero" class="light-bg img-bg-bottom img-bg-softer"
         style="background-image: url(/assets/images/art/slider03.jpg);">
    <div class="container inner">

        <div class="row">
            <div class="col-md-8 col-sm-9">
                <header>
                    <h1>Leerdoelen CMS</h1>
                    <p>Via deze contentmanager kun je leerdoelen toevoegen.</p>
                </header>
            </div><!-- /.col -->
        </div><!-- ./row -->

    </div><!-- /.container -->
</section>