@extends('layouts.error')

@section('content')

    <section id="pattern-background-2" class="dark-bg img-bg-softer"
             style="background-image: url(assets/images/art/pattern-background02.png);">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-9 inner center-block text-center">
                    <header>

                        <h1>404 - Er gaat iets fout...</h1>

                        <img src="{{ asset('/assets/images/art/404.gif') }}">

                    </header>
                    <a href="{{ route('get.home') }}" class="btn btn-large">Naar de homepage!</a>

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

@endsection