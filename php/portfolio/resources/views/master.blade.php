<!DOCTYPE html>
<html lang="nl">
<head>
    <title>wderoode{{ isset($title) ? ' - ' . $title : '' }}</title>

    @include('layouts.head_meta')

    @include('layouts.head_styles')

    @include('layouts.head_fonts')
</head>

<body>
<header>
    @include('layouts.navbar')
</header>

<main>
    @yield('content')
</main>

<footer class="dark-bg">
    @include('layouts.footer')
</footer>

@include('layouts.footer_scripts')
</body>
</html>