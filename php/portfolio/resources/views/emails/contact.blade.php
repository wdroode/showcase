<p>Formulier ingevuld op: {{ date('d-m-Y H:i') }}</p>

<table>
    <tr>
        <th>Naam:</th>
        <td>{{ $request->get('name') }}</td>
    </tr>
    <tr>
        <th>E-mail:</th>
        <td>{{ $request->get('email') }}</td>
    </tr>
    <tr>
        <th>Onderwerp:</th>
        <td>{{ $request->get('subject') }}</td>
    </tr>
    <tr>
        <th>Bericht:</th>
        <td>{{ $request->get('message') }}</td>
    </tr>
</table>

<p><small>Dit bericht is automatisch verstuurd.</small></p>