@extends('master')

@section('content')

    @include('auth.account.partials.hero')

    @include('auth.account.partials.form')

@endsection