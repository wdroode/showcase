<section id="pattern-background-1" class="light-bg img-bg-softer" style="background-image: url(assets/images/art/pattern-background01.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-9 inner center-block text-center">
                <header>
                    <h1>{{ isset($title) ? $title : 'Mijn account' }}</h1>
                    <p>{{ isset($description) ? $description : 'Verander hier je details!' }}</p>
                </header>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>