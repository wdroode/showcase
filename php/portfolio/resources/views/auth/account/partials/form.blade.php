<section id="contact-form">
    <div class="container inner">

        <div class="row">
            <div class="col-sm-12">
                <div class="row">

                    <div class="col-sm-6 col-sm-offset-3">

                        <h2>Wachtwoord veranderen</h2>

                        @include('tools.message')

                        <form id="contact-form" class="forms" action="{{ route('post.contact') }}" method="post">
                            {!! csrf_field() !!}

                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Wachtwoord...">
                                </div><!-- /.col -->
                            </div><!-- /.row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Herhaal wachtwoord...">
                                </div><!-- /.col -->
                            </div><!-- /.row -->

                            <button type="submit" class="btn btn-default btn-submit">Opslaan!</button>

                        </form>
                    </div><!-- ./col -->

                </div><!-- /.row -->
            </div><!-- /.col -->
        </div><!-- /.row -->

    </div><!-- /.container -->
</section>