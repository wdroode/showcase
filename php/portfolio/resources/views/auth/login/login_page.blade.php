@extends('master')

@section('content')

    @include('auth.login.partials.hero')

    @include('auth.login.partials.login_form')
@endsection