<section id="hero" class="light-bg img-bg img-bg-softer"
         style="background-image: url(assets/images/art/image-background04.jpg);">
    <div class="container inner">
        <div class="row">
            <div class="col-md-8">
                <header>
                    <h1>Verborgen content</h1>
                    <p>Deze content is bedoeld voor docenten! Log in om verder te gaan.</p>
                </header>
            </div><!-- /.col -->
        </div><!-- ./row -->
    </div><!-- /.container -->
</section>