<div class="container inner">
    <div class="row">

        <div class="col-md-6 inner-right inner-bottom-md">
            <section id="login-form">

                @if(isset($title))
                    <h2>{{ $title }}</h2>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        Inloggen mislukt, controleer je invoer!
                    </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                    {!! csrf_field() !!}

                    <div class="row">
                        <div class="col-md-8">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   placeholder="E-mailadres">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <input type="password" class="form-control" name="password" placeholder="Wachtwoord">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Inloggen</button>

                </form>
            </section>
        </div><!-- ./col -->

        <div class="col-md-6">
            <section id="contact-names" class="light-bg inner-xs inner-left-xs inner-right-xs">
                <div>
                    <h3 class="team-headline sidelines text-center"><span>Informatie</span></h3>

                    <p>
                        Hiernaast kunt u inloggen op wderoode.nl. Indien u nog geen account heeft, kunt u zich hier
                        <a href="{{ route('get.contact') }}" class="txt-btn">aanmelden</a>
                    </p>

                    <p>
                        Met een dergelijk account heeft u toegang tot mijn SWOT-analyse, leerdoelen en
                        reflectieverslagen.
                    </p>
                </div>
            </section>
        </div><!-- /.col -->

    </div><!-- /.row -->
</div><!-- /.container -->