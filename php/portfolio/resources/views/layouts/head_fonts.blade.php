<link href="http://fonts.googleapis.com/css?family=Lato:400,900,300,700" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic,700italic" rel="stylesheet">

<!-- Icons/Glyphs -->
<link href="{{ asset('/assets/fonts/fontello.css') }}" rel="stylesheet">

<!-- Favicon -->
<link rel="shortcut icon" href="{{ asset('/assets/images/favicon.ico') }}">

<!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
<!--[if lt IE 9]>
<script src="{{ asset('/assets/js/html5shiv.js') }}"></script>
<script src="{{ asset('/assets/js/respond.min.js') }}"></script>
<![endif]-->