<!DOCTYPE html>
<html lang="nl">
<head>
    <title>wderoode - 404</title>

    @include('layouts.head_meta')

    @include('layouts.head_styles')

    @include('layouts.head_fonts')
</head>

<body>

<main>
    @yield('content')
</main>

<footer class="dark-bg">
    <div class="container inner">
        <div class="row">

            <div class="col-md-4 col-sm-6 inner">
                <h4>Wie ben ik</h4>
                <a href="{{ route('get.home') }}"><img class="logo img-intext"
                                                       src="{{ asset('/assets/images/logo-white.png') }}" alt=""></a>
                <p class="text-justify">Mijn naam is Wesley de Roode en ik ben student aan de Hogeschool Rotterdam. Ik volg
                    hier de bachelor
                    Informatica in voltijd, waarvoor ik inmiddels mijn propedeuse heb behaald.</p>
                <a href="{{ route('get.home', ['#about']) }}" class="txt-btn">Lees meer</a>

            </div><!-- /.col -->


            <div class="col-md-4 col-sm-6 inner">
                <h4>Neem contact op</h4>
                <p class="text-justify">Heb je vragen? Neem gerust contact op! Je vraag wordt zo snel mogelijk
                    beantwoord.</p>
                <ul class="contacts">
                    <li><i class="icon-location contact"></i> Neherkade 1176, s-Gravenhage, 2521RA</li>
                    <li><i class="icon-mobile contact"></i> +316 4851 8877</li>
                    <li><a href="mailto:info@wderoode.nl"><i class="icon-mail-1 contact"></i> info@wderoode.nl</a></li>
                </ul><!-- /.contacts -->
            </div><!-- /.col -->

            <div class="col-md-4 col-sm-6 inner">
                <h4>Nieuwsbrief</h4>
                <p class="text-justify">Als eerste op de hoogte zijn van de nieuwste ontwikkelingen? Meld je dan aan voor
                    mijn nieuwsbrief.</p>
                <form id="newsletter" class="form-inline newsletter" role="form">
                    <label class="sr-only" for="exampleInputEmail">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail" placeholder="E-mailadres">
                    <button type="submit" class="btn btn-default btn-submit">Subscribe</button>
                </form>
            </div><!-- /.col -->

        </div><!-- /.row -->
    </div><!-- .container -->

    <div class="footer-bottom">
        <div class="container inner">
            <p class="pull-left">© Wesley de Roode. All rights
                reserved.</p>
            <ul class="footer-menu pull-right">
                <li><a href="{{ route('get.home') }}">Home</a></li>
                <li><a href="{{ route('get.portfolio') }}">Portfolio</a></li>
                <li><a href="{{ route('get.goals') }}">Leerdoelen</a></li>
                <li><a href="{{ route('get.swot') }}">SWOT-analyse</a></li>
                <li><a href="{{ route('get.contact') }}">Contact</a></li>
            </ul><!-- .footer-menu -->
        </div><!-- .container -->
    </div><!-- .footer-bottom -->
</footer>

@include('layouts.footer_scripts')
</body>
</html>