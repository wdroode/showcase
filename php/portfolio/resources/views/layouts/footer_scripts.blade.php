<!-- JavaScripts -->
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.easing.1.3.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.form.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/js/bootstrap-hover-dropdown.min.js') }}"></script>
<script src="{{ asset('/assets/js/skrollr.min.js') }}"></script>
<script src="{{ asset('/assets/js/skrollr.stylesheets.min.js') }}"></script>
<script src="{{ asset('/assets/js/waypoints.min.js') }}"></script>
<script src="{{ asset('/assets/js/waypoints-sticky.min.js') }}"></script>
<script src="{{ asset('/assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.isotope.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.easytabs.min.js') }}"></script>
<script src="{{ asset('/assets/js/google.maps.api.v3.js') }}"></script>
<script src="{{ asset('/assets/js/viewport-units-buggyfill.js') }}"></script>
<script src="{{ asset('/assets/js/scripts.js') }}"></script>