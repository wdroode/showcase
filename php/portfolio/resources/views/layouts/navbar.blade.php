<div class="navbar">

    <div class="navbar-header">
        <div class="container">

            <ul class="info pull-left">
                <li><a href="mailto:info@wderoode.nl"><i class="icon-mail-1 contact"></i> info@wderoode.nl</a></li>
                <li><i class="icon-mobile contact"></i> +316 4851 8877</li>
            </ul><!-- /.info -->

            <ul class="social pull-right">
                <li><a href="{{ $social['facebook'] }}"><i class="icon-s-facebook"></i></a></li>
                <li><a href="{{ $social['google'] }}"><i class="icon-s-gplus"></i></a></li>
                <li><a href="{{ $social['github'] }}"><i class="icon-s-github"></i></a></li>
                <li><a href="{{ $social['pinterest'] }}"><i class="icon-s-pinterest"></i></a></li>
                <li><a href="{{ $social['linkedin'] }}"><i class="icon-s-linkedin"></i></a></li>
            </ul><!-- /.social -->

            <a class="navbar-brand" href="{{ route('get.home') }}"><img src="{{ asset('/assets/images/logo.png') }}"
                                                                        class="logo" alt="W. de Roode als logo"></a>

            <a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i
                        class='icon-menu-1'></i></a>

        </div><!-- /.container -->
    </div><!-- /.navbar-header -->

    <div class="yamm">
        <div class="navbar-collapse collapse">
            <div class="container">

                <a class="navbar-brand" href="{{ route('get.home') }}"><img src="{{ asset('/assets/images/logo.png') }}"
                                                                            class="logo" alt="W. de Roode als logo"></a>


                <ul class="nav navbar-nav">

                    <li><a href="{{ route('get.home') }}">Home</a></li>
                    <li><a href="{{ route('get.portfolio') }}">Portfolio</a></li>
                    {{--<li><a href="{{ route('get.blog') }}">Blog</a></li>--}}
                    {{--<li><a href="{{ route('get.cv') }}">CV</a></li>--}}
                    <li><a href="{{ route('get.goals') }}">Leerdoelen</a></li>
                    <li><a href="{{ route('get.swot') }}">SWOT-analyse</a></li>
                    <li><a href="{{ route('get.contact') }}">Contact</a></li>

                    {{--<li class="dropdown pull-right searchbox">--}}
                    {{--<a href="#" class="dropdown-toggle js-activated"><i class="icon-search"></i></a>--}}

                    {{--<div class="dropdown-menu">--}}
                    {{--<form id="search" class="navbar-form search" role="search">--}}
                    {{--<input type="search" class="form-control" placeholder="Type to search">--}}
                    {{--<button type="submit" class="btn btn-default btn-submit icon-right-open"></button>--}}
                    {{--</form>--}}
                    {{--</div><!-- /.dropdown-menu -->--}}
                    {{--</li><!-- /.searchbox -->--}}


                    <ul class="nav navbar-nav navbar-right">
                        @if( ! Auth::check())
                            {{--<li><a href="{{ route('get.auth.login') }}"><i class="icon-user"></i> Login</a></li>--}}
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle js-activated"><i
                                            class="icon-user"></i> {{ $user->first_name }}<span
                                            class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    {{--<li><a href="{{ route('get.account') }}">Mijn account</a></li>--}}
                                    <li><a href="{{ route('get.auth.logout') }}">Uitloggen</a></li>
                                </ul>
                            </li>
                        @endif
                    </ul>

                </ul><!-- /.nav -->

            </div><!-- /.container -->
        </div><!-- /.navbar-collapse -->
    </div><!-- /.yamm -->
</div><!-- /.navbar -->