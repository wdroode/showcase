<!-- Stylesheets -->
<link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/css/main.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/css/blue.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/css/owl.carousel.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/css/owl.transitions.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/css/animate.min.css') }}" rel="stylesheet">