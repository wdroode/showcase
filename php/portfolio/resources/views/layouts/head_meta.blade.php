<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
@if(isset($description))
    <meta name="description" content="{{ $description }}">
@endif
<meta name="author" content="Wesley de Roode">