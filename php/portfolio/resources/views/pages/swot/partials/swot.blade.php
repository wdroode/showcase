<section id="swot-analyse">
    <div class="container inner">

        <div class="row swot-table">

            <div class="col-md-6 col-sm-6 inner-top-sm">
                <div class="plan">

                    <header>
                        <h2>Sterktes</h2>
                        <p>(Intern)</p>
                    </header>

                    <ul class="features">
                        <li>Motivatie</li>
                        <li>Vooropleiding</li>
                        <li>Werkervaring</li>
                        <li>Leeftijd</li>
                    </ul><!-- /.features -->

                </div><!-- /.plan -->
            </div><!-- /.col -->

            <div class="col-md-6 col-sm-6 inner-top-sm">
                <div class="plan">

                    <header>
                        <h2>Zwaktes</h2>
                        <p>(Intern)</p>
                    </header>

                    <ul class="features">
                        <li>Concentratie</li>
                        <li>Wiskunde</li>
                        <li>Nauwkeurigheid</li>
                        <li>Leeftijd</li>
                    </ul><!-- /.features -->

                </div><!-- /.plan -->
            </div><!-- /.col -->

            <div class="col-md-6 col-sm-6 inner-top-sm">
                <div class="plan">

                    <header>
                        <h2>Kansen</h2>
                        <p>(Extern)</p>
                    </header>

                    <ul class="features">
                        <li>Bijbaan</li>
                        <li>Eigen bedrijf</li>
                        <li>-</li>
                        <li>-</li>
                    </ul><!-- /.features -->

                </div><!-- /.plan -->
            </div><!-- /.col -->


            <div class="col-md-6 col-sm-6 inner-top-sm">
                <div class="plan">

                    <header>
                        <h2>Bedreigingen</h2>
                        <p>(Extern)</p>
                    </header>

                    <ul class="features">
                        <li>Ongemotiveerde teamgenoten</li>
                        <li>Slecht werk van teamgenoten</li>
                        <li>Slechte lessen</li>
                        <li>-</li>
                    </ul><!-- /.features -->

                </div><!-- /.plan -->
            </div><!-- /.col -->

        </div><!-- /.row -->

    </div><!-- /.container -->
</section>