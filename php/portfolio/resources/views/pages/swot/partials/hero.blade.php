<section id="pattern-background-2" class="dark-bg img-bg-softer" style="background-image: url(assets/images/art/pattern-background02.png);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-9 inner center-block text-center">
                <header>
                    <h1>{{ isset($title) ? $title : 'SWOT-Analyse' }}</h1>
                    <p>{{ isset($subtitle) ? $subtitle : '' }}</p>
                </header>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>