@extends('master')

@section('content')

    @include('pages.swot.partials.hero')

    @include('pages.swot.partials.swot')

@endsection