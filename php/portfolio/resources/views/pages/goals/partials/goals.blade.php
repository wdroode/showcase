<div class="row">
    <div class="col-md-9 center-block">

        <div class="posts sidemeta">

            @foreach($goals as $goal)

                <div class="post format-{{ $goal->label->code }}">

                    <div class="date-wrapper">
                        <div class="date">
                            <span class="day">{{ $goal->created_at->format('d') }}</span>
                            <span class="month">{{ $goal->created_at->format('M') }}</span>
                        </div><!-- /.date -->
                    </div><!-- /.date-wrapper -->

                    <div class="format-wrapper">
                        <a href="#" data-filter=".format-{{ $goal->label->code }}"><i class="icon-{{ $goal->label->code }}"></i></a>
                    </div><!-- /.format-wrapper -->

                    <div class="post-content">

                        <h2 class="post-title">{{ $goal->title }}</h2>

                        <ul class="meta">
                            <li><i class="icon-tags"></i> {{ $goal->label->title }}</li>
                            <li>

                                @if($goal->reflection == null && $currentDate > $goal->deadline)
                                    <i class="icon-cancel-circled"></i> Status: Niet behaald
                                @elseif($goal->reflection == null && $currentDate < $goal->deadline)
                                    <i class="icon-cog-alt"></i> Status: Bezig
                                @else
                                    <i class="icon-ok-circle"></i> Status: Behaald
                                @endif
                            </li>
                            <li><i class="icon-calendar"></i> Deadline: {{ $goal->deadline->format('d-m-Y') }}</li>

                        </ul><!-- /.meta -->

                        <p>{{ $goal->description }}</p>

                        @if($goal->reflection != null)
                            <strong>Resultaat</strong>
                            <br>
                            <p>{{ $goal->reflection->text }}</p>
                        @endif

                        <p><em>Laatst gewijzigd op: <strong>{{ $goal->updated_at->format('d-m-Y') }}</strong></em>
                        </p>

                    </div><!-- /.post-content -->
                </div><!-- /.post -->

            @endforeach

        </div><!-- /.posts -->
    </div><!-- /.col -->
</div><!-- /.row -->