<div class="row inner-bottom-xs">
    <div class="col-sm-12">
        <ul class="format-filter text-center">
            <li><a class="active" href="#" data-filter="*" title="Alles" data-rel="tooltip" data-placement="top"><i class="icon-th"></i></a></li>

            @foreach($labels as $label)
                    <li><a href="#" data-filter=".format-{{ $label->code }}" title="{{ $label->title }}" data-rel="tooltip" data-placement="top"><i class="icon-{{ $label->code }}"></i></a></li>
                @endforeach

        </ul><!-- /.format-filter -->
    </div><!-- /.col -->
</div><!-- /.row -->