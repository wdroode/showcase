<section id="pattern-background-3" class="tint-bg img-bg-softer"
         style="background-image: url(assets/images/art/pattern-background03.png);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-9 inner center-block text-center">
                <header>
                    <h1>{{ isset($title) ? $title : 'Leerdoelen' }}</h1>
                    <p>{{ isset($subtitle) ? $subtitle : '' }}</p>
                </header>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>