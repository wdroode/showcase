@extends('master')

@section('content')

    @include('pages.goals.partials.hero')

    <section id="goals" class="light-bg">
        <div class="container inner-top-sm inner-bottom classic-blog no-sidebar">

            @include('pages.goals.partials.filter')

            @include('pages.goals.partials.goals')

        </div><!-- /.container -->
    </section>

@endsection