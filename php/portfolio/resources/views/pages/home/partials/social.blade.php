<section id="share" class="light-bg">
    <div class="row">

        <div class="col-sm-4 reset-padding-right">
            <a href="{{ $social['facebook'] }}" class="btn-share-md">
                <p class="name">Facebook</p>
                <i class="icon-s-facebook"></i>
            </a>
        </div><!-- /.col -->

        <div class="col-sm-4 reset-padding">
            <a href="{{ $social['google'] }}" class="btn-share-md">
                <p class="name">Google +</p>
                <i class="icon-s-gplus"></i>
            </a>
        </div><!-- /.col -->

        <div class="col-sm-4 reset-padding-left">
            <a href="{{ $social['linkedin'] }}" class="btn-share-md">
                <p class="name">LinkedIn</p>
                <i class="icon-s-linkedin"></i>
            </a>
        </div><!-- /.col -->

    </div><!-- /.row -->
</section>