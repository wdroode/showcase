<div class="col-md-8 inner-right-md">

    <section id="hero">
        <header>
            <h1>Wie ben ik? – <br>student & IT'er</h1>
        </header>
    </section>

    <section id="who-we-are" class="inner-top-sm">
        <div class="row">

            <div class="col-sm-6 inner-right-xs inner-bottom-sm">
                <h2>Over mezelf</h2>

                <p class="text-justify">
                    Mij naam is Wesley de Roode, ik ben 27 jaar oud en afkomstig uit de stad 's-Gravenhage. Na mijn
                    MBO opleiding tot systeem- en netwerkbeheerder, ben ik in 2010 mijn IT carrière begonnen bij <a href="http://www.pdc.nl/">PDC</a>.
                    <br>
                    Gedurende mijn tijd bij PDC heb ik veel geleerd, heb ik mezelf verder
                    kunnen ontwikkelen en is ook mijn interesse voor programmeren ontstaan. Deze interesse is
                    uiteindelijk een hobby geworden en mijn volgende stap is om er mijn werk van te maken. Daarom ben ik
                    in 2015 aan de bachelor Informatica op de Hogeschool Rotterdam begonnen.
                </p>
            </div><!-- /.col -->

            <div class="col-sm-6 inner-left-xs inner-bottom-sm">
                <h2>Mijn toekomstvisie</h2>

                <p class="text-justify">
                    Van alle projecten waaraan ik heb meegewerkt vind ik die met betrekking tot webapplicaties het
                    leukst. In het afgelopen studiejaar heb ik daarom een keuzevak dot.net gevolgd, en houd ik mij in
                    mijn vrije tijd veel bezig met het bouwen van apps in PHP en het Laravel framework.
                    Na het behalen van mijn diploma wil ik graag aan de slag als back-end developer. Dit zal in het
                    begin bij een bedrijf zijn, maar later als ik voldoende kennis heb opgedaan hoop ik voor mezelf te
                    kunnen beginnen. Daarom zal ik me de komende jaren ook op het ondernemerschap richten.
                </p>

            </div><!-- /.col -->

        </div><!-- ./row -->
    </section>
</div>