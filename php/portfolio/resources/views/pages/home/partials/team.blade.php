<div class="col-md-4">

    <section id="team" class="light-bg inner-xs inner-left-xs inner-right-xs">

        <h3 class="team-headline sidelines text-center"><span>Smoelenboek</span></h3>

        <div class="row thumbs gap-md text-center">

            <div class="col-sm-12 thumb">
                <figure class="member">

                    <div class="member-image">

                        <div class="text-overlay">
                            <div class="info">
                                <ul class="social">
                                    <li><a href="{{ $social['facebook'] }}"><i class="icon-s-facebook"></i></a></li>
                                    <li><a href="{{ $social['google'] }}"><i class="icon-s-gplus"></i></a></li>
                                    <li><a href="{{ $social['linkedin'] }}"><i class="icon-s-linkedin"></i></a></li>
                                </ul><!-- /.social -->
                            </div><!-- /.info -->
                        </div><!-- /.text-overlay -->

                        <img src="{{ asset('/assets/images/pasfoto.jpg') }}">

                    </div><!-- /.member-image -->

                    <figcaption class="member-details bordered no-top-border">
                        <h3>
                            Wesley de Roode
                            <span>Eigenaar wderoode.nl</span>
                        </h3>
                    </figcaption>

                </figure>
            </div><!-- /.col -->

        </div><!-- /.row -->

    </section>
</div>