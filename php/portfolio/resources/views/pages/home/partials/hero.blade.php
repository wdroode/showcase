<section id="hero">
    <div id="owl-main" class="owl-carousel height-lg owl-inner-nav owl-ui-lg">

        <div class="item" style="background-image: url(assets/images/art/slider04.jpg);">
            <div class="container">
                <div class="caption vertical-center text-right">

                    <h1 class="fadeIn-1 dark-bg light-color">
                        <span>{{ isset($title) ? $title : 'Wesley de Roode' }}</span></h1>
                    <p class="fadeIn-2 light-color">{{ isset($subtitle) ? $subtitle : '' }}</p>
                    <div class="fadeIn-3">
                        <a href="{{ route('get.portfolio') }}" class="btn btn-large">Naar portfolio</a>
                    </div><!-- /.fadeIn -->

                </div><!-- /.caption -->
            </div><!-- /.container -->
        </div><!-- /.item -->

        <div class="item" style="background-image: url(assets/images/art/slider01.jpg);">
            <div class="container">
                <div class="caption vertical-center text-center">

                    <h1 class="fadeInDown-1 light-color">Welkom <br>op wderoode.nl</h1>
                    <p class="fadeInDown-2 medium-color">Neem ook een kijkje naar mijn andere projecten.</p>
                    <div class="fadeInDown-3">
                        <a href="{{ route('get.portfolio') }}" class="btn btn-large">Naar portfolio</a>
                    </div><!-- /.fadeIn -->

                </div><!-- /.caption -->
            </div><!-- /.container -->
        </div><!-- /.item -->

        <div class="item" style="background-image: url(assets/images/art/slider03.jpg);">
            <div class="container">
                <div class="caption vertical-center text-left">

                    <h1 class="fadeInRight-1 dark-bg light-color"><span>Gemaakt in <br>het Laravel framework</span></h1>
                    <p class="fadeInRight-2 dark-color"><br>Deze site is geprogrammeerd in Laravel met PHP & JavaScript.
                    </p>
                    <div class="fadeInRight-3">
                        <a href="https://laravel.com/" class="btn btn-large">Laravel</a>
                    </div><!-- /.fadeIn -->

                </div><!-- /.caption -->
            </div><!-- /.container -->
        </div><!-- /.item -->

    </div><!-- /.owl-carousel -->
</section>