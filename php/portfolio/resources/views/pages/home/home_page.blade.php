@extends('master')

@section('content')

    @include('pages.home.partials.hero')

    <div class="container inner" id="about">
        <div class="row">

            @include('pages.home.partials.features')

            @include('pages.home.partials.team')

        </div><!-- /.row -->
    </div><!-- /.container -->

    @include('pages.home.partials.social')

@endsection