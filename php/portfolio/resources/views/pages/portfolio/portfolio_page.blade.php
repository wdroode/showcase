@extends('master')

@section('content')

    <section id="portfolio">

        @include('pages.portfolio.partials.hero')

        @include('pages.portfolio.partials.portfolio')

        @include('pages.portfolio.partials.modal')

    </section>

@endsection

