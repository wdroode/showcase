@foreach($projects as $project)

    <div class="modal fade" id="{{ $project->category->title . '-' . $project->id }}" tabindex="-1" role="dialog" aria-labelledby="{{ $project->title . '-' . $project->id }}"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true"><i class="icon-cancel-1"></i></span></button>
                    <h4 class="modal-title" id="modal-work03">{{ $project->title }}</h4>
                </div><!-- /.modal-header -->

                <div class="modal-body">

                    <section id="portfolio-post">
                        <div class="container inner-top-xs inner-bottom">

                            <div class="row">
                                <div class="col-sm-12">
                                    <img class="img-responsive" src="{{ $project->img_url }}">
                                </div><!-- /.col -->
                            </div><!-- /.row -->

                            <div class="row inner-top-xs reset-xs">

                                <div class="col-sm-7 inner-top-xs inner-right-xs">
                                    <header>
                                        <h2>Over het project</h2>
                                        <p class="text-normal"><strong>{{ $project->description }}</strong></p>
                                        <p class="text-normal">{{ $project->text }}</p>
                                    </header>
                                </div><!-- /.col -->

                                <div class="col-sm-4 col-sm-offset-1 outer-top-xs inner-left-xs border-left">
                                    <ul class="item-details">
                                        <li class="date">{{ $project->delivery_date->format('d-m-Y') }}</li>
                                        <li class="categories">{{ $project->category->title }}</li>
                                        <li class="client">{{ $project->client }}</li>
                                        <li class="url"><a href="{{ $project->github }}">GitHub</a>
                                        </li>
                                    </ul><!-- /.item-details -->

                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>
                </div><!-- /.modal-body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div><!-- /.modal-footer -->

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endforeach