<ul class="portfolio-filter filter text-center">
    <li><a href="#" data-filter="*" class="active">All</a></li>
    @foreach($categories as $category)
        <li><a href="#" data-filter=".{{ $category->title }}">{{ $category->title }}</a></li>
    @endforeach

</ul><!-- /.filter -->