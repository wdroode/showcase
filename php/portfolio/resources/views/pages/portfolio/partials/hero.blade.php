<section id="image-background-3" class="tint-bg img-bg-soft img-bg" style="background-image: url(assets/images/art/image-background03.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-9 inner center-block text-center">
                <header>
                    <h1>{{ isset($title) ? $title : 'Mijn portfolio' }}</h1>
                    <p>{{ isset($subtitle) ? $subtitle : '' }}</p>
                </header>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>