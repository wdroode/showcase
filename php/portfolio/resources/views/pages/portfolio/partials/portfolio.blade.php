<div class="container inner-bottom">
    <div class="row">
        <div class="col-sm-12 portfolio">

            @include('pages.portfolio.partials.filter')

            <ul class="items col-3 gap">

                @foreach($projects as $project)
                    <li class="item thumb {{ $project->category->title }}">
                        <figure>

                            <div class="icon-overlay icn-link">
                                <a href="#{{ $project->category->title . '-' . $project->id }}" data-toggle="modal"><img class="img-responsive" src="{{ $project->thumb_url }}" alt=""></a>
                            </div><!-- /.icon-overlay -->

                            <figcaption class="bordered no-top-border">
                                <div class="info">
                                    <h4><a href="#{{ $project->category->title . '-' . $project->id }}" data-toggle="modal">{{ $project->title }}</a></h4>
                                    <p>{{ $project->category->title }}</p>
                                </div><!-- /.info -->
                            </figcaption>

                        </figure>
                    </li><!-- /.item -->
                @endforeach

            </ul><!-- /.items -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.container -->
