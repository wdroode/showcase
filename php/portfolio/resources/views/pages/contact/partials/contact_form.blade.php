<section id="contact-form">
    <div class="container inner">

        <div class="row">
            <div class="col-sm-12">
                <div class="row">

                    <div class="col-sm-6 outer-top-md inner-right-sm">

                        <h2>Stuur een bericht</h2>

                        @include('tools.message')

                        <form id="contact-form" class="forms" action="{{ route('post.contact') }}" method="post">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}"
                                           placeholder="Naam (vereist)">
                                </div><!-- /.col -->
                            </div><!-- /.row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}"
                                           placeholder="E-mailadres (vereist)">
                                </div><!-- /.col -->
                            </div><!-- /.row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" name="subject" id="subject" class="form-control" value="{{ old('subject') }}"
                                           placeholder="Onderwerp">
                                </div><!-- /.col -->
                            </div><!-- /.row -->

                            <div class="row">
                                <div class="col-sm-12">
                                        <textarea name="message" id="message" class="form-control" value="{{ old('message') }}"
                                                  placeholder="Je bericht ..."></textarea>
                                </div><!-- /.col -->
                            </div><!-- /.row -->

                            <button type="submit" class="btn btn-default btn-submit">Bericht verzenden</button>

                        </form>
                    </div><!-- ./col -->

                    <div class="col-sm-6 outer-top-md inner-left-sm border-left">

                        <h2>Contactgegevens</h2>
                        <p>Liever op een andere manier contact opnemen? Zie mijn contactgegevens hieronder.</p>

                        <h3>Wesley de Roode</h3>
                        <ul class="contacts">
                            <li><i class="icon-location contact"></i> Neherkade 1176, s-Gravenhage, 2521RA</li>
                            <li><i class="icon-mobile contact"></i> +316 4851 8877</li>
                            <li><a href="mailto:info@wderoode.nl"><i class="icon-mail-1 contact"></i> info@wderoode
                                    .nl</a>
                            </li>
                        </ul><!-- /.contacts -->

                        <div class="social-network">
                            <h3>Social media</h3>
                            <ul class="social">
                                <li><a href="{{ $social['facebook'] }}"><i class="icon-s-facebook"></i></a></li>
                                <li><a href="{{ $social['google'] }}"><i class="icon-s-gplus"></i></a></li>
                                <li><a href="{{ $social['github'] }}"><i class="icon-s-github"></i></a></li>
                                <li><a href="{{ $social['pinterest'] }}"><i class="icon-s-pinterest"></i></a></li>
                                <li><a href="{{ $social['linkedin'] }}"><i class="icon-s-linkedin"></i></a></li>
                            </ul><!-- /.social -->
                        </div><!-- /.social-network -->

                    </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.col -->
        </div><!-- /.row -->

    </div><!-- /.container -->
</section>