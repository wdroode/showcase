@extends('master')

@section('content')

    @include('pages.contact.partials.hero')

    @include('pages.contact.partials.contact_form')

    @include('pages.contact.partials.google_maps')

@endsection