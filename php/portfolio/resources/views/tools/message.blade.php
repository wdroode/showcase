@if (session('status'))
    <div class="alert alert-dismissible alert-{{ session('status_type') }}" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        @if(session('status_title'))
            <strong>{{ session('status_title') }}</strong>
        @endif
        {{ session('status') }}
    </div>
@endif