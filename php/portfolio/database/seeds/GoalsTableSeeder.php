<?php

use Illuminate\Database\Seeder;

use App\Wderoode\Models\Goal;

use Carbon\Carbon;

class GoalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $goal = Goal::create([
            'title' => 'Planningen maken',
            'description' => 'Aan het eind van het blok wil ik in staat zijn goede planningen te maken betreft het huiswerk. Dit leerdoel ga ik realiseren door wekelijkse een planning te maken, en deze te reviewen aan het eind van de betreffende week.',
            'deadline' => Carbon::createFromDate(2015, 2, 7)->toDateTimeString(),
            'label_id' => 2,
        ]);

        $goal = Goal::create([
            'title' => 'Concentratie verbeteren',
            'description' => 'Doordat het vaak rumoerig is in de klas kan ik mij lastig concentreren. Hierdoor moet ik thuis vaak huiswerk inhalen omdat ik er in de les niet aan het kunnen werken. Als leerdoel heb ik voor het volgende blok dat ik me beter wil leren concentreren. Hoe ik dit precies ga doen dat heb ik nog niet bedacht, dat zal onderdeel uitmaken van mijn leerdoel.',
            'deadline' => Carbon::createFromDate(2015, 2, 7)->toDateTimeString(),
            'label_id' => 3,
        ]);

        $goal = Goal::create([
            'title' => 'PHP',
            'description' => 'Omdat ik graag nieuwe programmeertalen wil leren, ga ik me het volgende blok bezig houden met PHP. Dit leerdoel ga ik realiseren door een online cursus te volgen en een webcrawler te maken. Aan het eind van het blok hoop ik mijn leerdoel behaald te hebben, dit is gelukt als de webcrawler af is.',
            'deadline' => Carbon::createFromDate(2015, 4, 24)->toDateTimeString(),
            'label_id' => 1,
        ]);

        $goal = Goal::create([
            'title' => '.NET',
            'description' => 'Omdat ik graag nieuwe programmeertalen wil leren, ga ik me het volgende blok bezig houden met .NET, Dit leerdoel ga ik realiseren door het keuzevak \'Programmeren in dotnet\' te volgen. Het keuzevak duurt 1 blok, dus aan het eind van het blok zal ik mijn leerdoel behaald hebben.',
            'deadline' => Carbon::createFromDate(2015, 11, 15)->toDateTimeString(),
            'label_id' => 1,
        ]);

        $goal = Goal::create([
            'title' => 'PHP, Javascript & Laravel',
            'description' => 'Als voorbereiding op mijn stage als back-end developer wil ik mijn kennis betreft PHP verrijken, basiskennis Javascript opdoen en het Laravel framework verkennen. Dit leerdoel wil ik oppakken door mijn portfolio opnieuw te bouwen en deze aan te vullen met een authenticatiesysteem en een content manager. Ik hoop mijn nieuwe portfolio aan het eind van dit schooljaar af te ronden en hiermee mijn leerdoel te behalen.',
            'deadline' => Carbon::createFromDate(2016, 4, 29)->toDateTimeString(),
            'label_id' => 1,
        ]);

    }

}
