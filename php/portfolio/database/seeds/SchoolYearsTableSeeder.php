<?php

use Illuminate\Database\Seeder;

use App\Wderoode\Models\SchoolYear;

use Carbon\Carbon;

class SchoolYearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schoolYear = SchoolYear::create([
           'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
           'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
        ]);

        $schoolYear = SchoolYear::create([
           'start_date' => Carbon::createFromDate(2015, 8, 31)->toDateTimeString(),
           'end_date' => Carbon::createFromDate(2016, 7, 25)->toDateTimeString(),
        ]);

        $schoolYear = SchoolYear::create([
           'start_date' => Carbon::createFromDate(2016, 8, 31)->toDateTimeString(),
           'end_date' => Carbon::createFromDate(2017, 7, 25)->toDateTimeString(),
        ]);

        $schoolYear = SchoolYear::create([
           'start_date' => Carbon::createFromDate(2017, 8, 31)->toDateTimeString(),
           'end_date' => Carbon::createFromDate(2018, 7, 25)->toDateTimeString(),
        ]);
    }
}
