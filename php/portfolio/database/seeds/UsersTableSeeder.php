<?php

use Illuminate\Database\Seeder;

use App\Wderoode\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'Wesley',
            'last_name' => 'de Roode',
            'email' => 'w.de.roode@live.nl',
            'password' => Hash::make('password'),
        ]);

        $user = User::create([
            'first_name' => 'Sandra',
            'last_name' => 'Maikoe',
            'email' => 's.maikoe@gmail.com',
            'password' => Hash::make('password'),
        ]);

        $user = User::create([
            'first_name' => 'Wendy',
            'last_name' => 'van Ijperen',
            'email' => 'ijpew@hr.nl',
            'password' => Hash::make('hTUY6cHp'),
        ]);
    }

}
