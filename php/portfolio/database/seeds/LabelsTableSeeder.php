<?php

use Illuminate\Database\Seeder;

use App\Wderoode\Models\Label;

class LabelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $label = Label::create([
            'title' => 'Kennis',
            'code' => 'code',
        ]);

        $label = Label::create([
            'title' => 'Vaardigheden',
            'code' => 'graduation-cap',
        ]);

        $label = Label::create([
            'title' => 'Houding',
            'code' => 'user',
        ]);
    }
}
