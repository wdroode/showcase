<?php

use Illuminate\Database\Seeder;

use App\Wderoode\Models\Reflection;

class ReflectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reflection = Reflection::create([
            'text' => 'Gedurende het blok heb ik iedere week een planning gemaakt voor het maken van mijn huiswerk. Hiervoor heb ik gebruik gemaakt van de Google agenda zodat deze zowel op de pc als op mijn smartphone te bekijken is. Ik weet nu precies in te schatten hoeveel tijd ik voor mijn huiswerk nodig heb, en deze zo in te plannen zodat ik niet in de knoei kom met mijn werktijden.',
            'goal_id' => 1,
        ]);

        $reflection = Reflection::create([
            'text' => 'Om me beter te kunnen concentreren op de lessen probeer ik nu zo ver mogelijk van de drukte vandaan een plek te vinden. Als het praktijkles betreft los ik mijn probleem op door muziek te luisteren, of eerder weg te gaan zodat ik bijvoorbeeld in de bibliotheek kan zitten.',
            'goal_id' => 2,
        ]);

        $reflection = Reflection::create([
            'text' => 'In plaats van het volgen van een online cursus heb ik gebruik gemaakt van het boek \'PHP & MySQL\'. Hiermee heb ik voldoende kennis opgedaan om de webcrawler te kunnen bouwen en heb ik nu de basiskennis onder de knie.',
            'goal_id' => 3,
        ]);

        $reflection = Reflection::create([
            'text' => 'Het keuzevak is goed bevallen, ik heb een voldoende gehaald en hiermee mijn leerdoel behaald.',
            'goal_id' => 4,
        ]);

        $reflection = Reflection::create([
            'text' => 'Zoals je ziet ben ik aardig op weg met het behalen van mijn leerdoel. Het enige dat ik nu nog moet bouwen om mijn nieuwe portfolio af te ronden is de content manager, deze hoop ik aan het eind van dit blok af te ronden.',
            'goal_id' => 5,
        ]);

    }
}
