<?php

use Illuminate\Database\Seeder;

use App\Wderoode\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::create([
            'title' => 'Java'
        ]);

        $category = Category::create([
            'title' => 'dotnet'
        ]);

        $category = Category::create([
            'title' => 'PHP'
        ]);

        $category = Category::create([
            'title' => 'HTML'
        ]);

        $category = Category::create([
            'title' => 'Android'
        ]);
    }
}
