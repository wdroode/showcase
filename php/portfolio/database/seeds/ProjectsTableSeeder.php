<?php

use Illuminate\Database\Seeder;

use App\Wderoode\Models\Project;

use Carbon\Carbon;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project = Project::create([
            'title' => 'GTA-Greenfoot',
            'description' => 'Standalone spel, gemaakt in het Java framework Greenfoot',
            'text' => 'Dit spel is gemaakt als projectopdracht voor de Haagse Hogeschool en ontwikkelt in het Java framework Greenfoot. Het spel is bedoeld als parodie op ‘Grand Theft Auto’ en heeft als doel het stelen en verkopen van auto’s.',
            'client' => 'via Haagse Hogeschool',
            'github' => 'https://github.com/wesrd/gtaGreenfoot',
            'img_url' => '/assets/images/projects/gtagreenfoot.png',
            'thumb_url' => '/assets/images/projects/gtagreenfoot-thumb.png',
            'category_id' => '1',
            'delivery_date' => Carbon::create(2013, 11, 2)
        ]);

        $project = Project::create([
            'title' => 'Speel-O-Theek',
            'description' => 'Standalone administratief systeem in Java',
            'text' => 'Dit systeem is gebouwd voor Speel-O-Theek Klapstuk in Leiden, een uitleencentrum van speelgoed. Het doel van het systeem is het bijhouden van de administratie in een MySQL database. De core features hierbij zijn het beheren van: speelgoed, uitleningen, lidmaatschappen en het uitdraaien van overzichten.
In dit project was ik verantwoordelijk voor de module speelgoedbeheer, het ontwerpen en bouwen van de database. Daarnaast heb ik nog een aantal tools ontwikkelt zoals het automatisch aanvullen van de zoekwoorden, query handlers voor het aanroepen voor de sql queries en het boetesysteem.',
            'client' => 'via Haagse Hogeschool',
            'github' => 'https://github.com/wesrd/Speel-O-Theek',
            'img_url' => '/assets/images/projects/speelotheek.png',
            'thumb_url' => '/assets/images/projects/speelotheek-thumb.png',
            'category_id' => '1',
            'delivery_date' => Carbon::create(2014, 1, 19)
        ]);

        $project = Project::create([
            'title' => 'Doolhof-game',
            'description' => 'Standalone spel gemaakt in Java',
            'text' => 'Dit spel is gemaakt als projectopdracht voor de Haagse Hogeschool en ontwikkelt in het Java. Het spel bestaat uit een doolhof die de speler moet doorlopen om te kunnen winnen. Dit doolhof wordt bij iedere start willekeurig gegenereerd en bevat ook monsters die moeten worden verslagen. Verdere eisen waaraan wij hebben voldaan waren het schrijven van unit tests, deze vind je terug in de projectbestanden. ',
            'client' => 'ForFun via Haagse Hogeschool',
            'github' => 'https://github.com/wesrd/doolhofgame',
            'img_url' => '/assets/images/projects/doolhofgame.png',
            'thumb_url' => '/assets/images/projects/doolhofgame-thumb.png',
            'category_id' => '1',
            'delivery_date' => Carbon::create(2014, 4, 10)
        ]);

        $project = Project::create([
            'title' => 'Filmpje',
            'description' => 'Online kaartjes verkoop in HTML & JavaScript',
            'text' => 'Deze webshop is ontwikkelt voor een fictieve bioscoop in Rotterdam. Het doel van het systeem is het online verkopen van bioscoopkaartjes met daarbij de mogelijkheid om plaatsen te kiezen. In dit project was ik verantwoordelijk voor het bouwen van het bestellingsproces bestaande uit: reserveringsysteem, genereren van het ticket en de betalingspagina.',
            'client' => 'Fictief bedrijf via Hogeschool Rotterdam',
            'github' => 'https://github.com/wesrd/Filmpje',
            'img_url' => '/assets/images/projects/filmpje.jpg',
            'thumb_url' => '/assets/images/projects/filmpje-thumb.jpg',
            'category_id' => '4',
            'delivery_date' => Carbon::create(2014, 11, 4)
        ]);

        $project = Project::create([
            'title' => 'Haven game',
            'description' => 'Serious game voor haven industrie in Java',
            'text' => 'Deze game is ontwikkelt als reclame voor de haven industrie in Rotterdam. Het spel bestaat uit 4 onderdelen die de verschillende facetten van de haven industrie afbeeld. In dit project was ik verantwoordelijk voor het spel haven termen, waarbij woorden van boven naar beneden vallen en de speler deze dient te typen voordat ze de onderkant van het scherm raken.',
            'client' => 'Fictief bedrijf via Hogeschool Rotterdam',
            'github' => 'https://github.com/wesrd/havengame',
            'img_url' => '/assets/images/projects/havengame.jpg',
            'thumb_url' => '/assets/images/projects/havengame-thumb.jpg',
            'category_id' => '1',
            'delivery_date' => Carbon::create(2015, 1, 22)
        ]);

        $project = Project::create([
            'title' => 'Markthal crawler',
            'description' => 'Twitter-, Facebook- en webcrawler in Java',
            'text' => 'Project 3 is een windows applicatie waarmee je overzichtelijk informatie verkrijgt over de Markthal. Deze informatie wordt gehaald van Twitter. Alle tweets over de Markhal worden opgeslagen in een database. Met deze informatie kan het programma analyses uitvoeren. Bijvoorbeeld: Het aantal positieve tweets bij goed en/of slecht weer, aantal tweets bij bepaalde evenementen, enz.',
            'client' => 'Fictief bedrijf via Hogeschool Rotterdam',
            'github' => 'https://github.com/wesrd/markthalcrawler',
            'img_url' => '/assets/images/projects/markthalcrawler.jpg',
            'thumb_url' => '/assets/images/projects/markthalcrawler-thumb.jpg',
            'category_id' => '1',
            'delivery_date' => Carbon::create(2015, 3, 4)
        ]);



        $project = Project::create([
            'title' => 'Bookshelf',
            'description' => 'Boekenmanager in Android',
            'text' => 'Bookshelf is een Android applicatie met diverse handige features. Je kan er gemakkelijk boeken mee toevoegen aan je account door middel van de barcode van een boek te scannen. Door vrienden toe te voegen kun je gemakkelijk naar elkaars \'boekenplank\' kijken. Je kan dan verzoeken doen om boeken te lenen. Daarnaast is het ook nog mogelijk om boeken te koop te zetten in de boekenmarkt.',
            'client' => 'Fictief bedrijf via Hogeschool Rotterdam',
            'github' => 'https://github.com/wesrd/bookshelf',
            'img_url' => '/assets/images/projects/bookshelf.jpg',
            'thumb_url' => '/assets/images/projects/bookshelf-thumb.jpg',
            'category_id' => '5',
            'delivery_date' => Carbon::create(2015, 6, 7)
        ]);

        $project = Project::create([
            'title' => 'CityGis',
            'description' => 'CSV parser in C++ en PHP',
            'text' => 'Deze webapplicatie is ontwikkelt als projectopdracht voor het bedrijf CityGis. Het doel van de applicatie is het verwerken en leesbaar maken van datastreams, en genereren van rapporten. In dit project heb ik zowel aan de front-end als de back-end gewerkt, en was ik verantwoordelijk voor de koppeling naar de database in C++.',
            'client' => 'CityGis bedrijf via Hogeschool Rotterdam',
            'github' => 'https://github.com/wesrd/project56',
            'img_url' => '/assets/images/projects/citygis.jpg',
            'thumb_url' => '/assets/images/projects/citygis-thumb.jpg',
            'category_id' => '3',
            'delivery_date' => Carbon::create(2016, 1, 26)
        ]);

        $project = Project::create([
            'title' => 'Bitshop',
            'description' => 'Webapplicatie met shop voor het verhandelen van bitcoins',
            'text' => 'Deze webapplicatie is gebouwd als eindopdracht voor het keuzevak "programmeren in .NET". Het doel van deze webapplicatie is het beheren van de koop en verkoop van bitcoins in combinatie met een webshop.',
            'client' => 'Fictief bedrijf via Hogeschool Rotterdam',
            'github' => 'https://github.com/wesrd/Bitshop',
            'img_url' => '/assets/images/projects/bitshop.png',
            'thumb_url' => '/assets/images/projects/bitshop-thumb.png',
            'category_id' => '2',
            'delivery_date' => Carbon::create(2016, 2, 12)
        ]);

    }
}
