<?php

use Illuminate\Database\Seeder;

use App\Wderoode\Models\SemesterGoal;

class SemesterGoalsTableSeeder extends Seeder
{

    public $timestamps = false;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $semesterGoal = SemesterGoal::create([
            'semester_id' => 1,
            'goal_id' => 1,
        ]);

        $semesterGoal = SemesterGoal::create([
            'semester_id' => 1,
            'goal_id' => 2,
        ]);

        $semesterGoal = SemesterGoal::create([
            'semester_id' => 5,
            'goal_id' => 3,
        ]);
    }
}
