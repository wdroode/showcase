<?php

use Illuminate\Database\Seeder;

use App\Wderoode\Models\Semester;

use Carbon\Carbon;

class SemestersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $semester = Semester::create([
            'nr' => 1,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 1,
        ]);

        $semester = Semester::create([
            'nr' => 2,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 1,
        ]);

        $semester = Semester::create([
            'nr' => 3,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 1,
        ]);

        $semester = Semester::create([
            'nr' => 4,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 1,
        ]);

        $semester = Semester::create([
            'nr' => 1,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 2,
        ]);

        $semester = Semester::create([
            'nr' => 2,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 2,
        ]);

        $semester = Semester::create([
            'nr' => 3,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 2,
        ]);

        $semester = Semester::create([
            'nr' => 4,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 2,
        ]);

        $semester = Semester::create([
            'nr' => 1,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 3,
        ]);

        $semester = Semester::create([
            'nr' => 2,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 3,
        ]);

        $semester = Semester::create([
            'nr' => 3,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 3,
        ]);

        $semester = Semester::create([
            'nr' => 4,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 3,
        ]);

        $semester = Semester::create([
            'nr' => 1,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 4,
        ]);

        $semester = Semester::create([
            'nr' => 2,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 4,
        ]);

        $semester = Semester::create([
            'nr' => 3,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 4,
        ]);

        $semester = Semester::create([
            'nr' => 4,
            'start_date' => Carbon::createFromDate(2014, 8, 31)->toDateTimeString(),
            'end_date' => Carbon::createFromDate(2015, 7, 25)->toDateTimeString(),
            'school_year_id' => 4,
        ]);
    }
}
